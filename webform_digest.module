<?php

module_load_include('inc', 'webform', 'includes/webform.export');

/**
 * Implementation of hook_menu().
 */
function webform_digest_menu() {
  $items = array();
  $items['node/%webform_menu/webform/digest'] = array(
    'title' => 'Digest',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webform_digest_form', 1),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
    'file' => 'webform_digest.inc',
    'weight' => 1,
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_webform_exporters().
 *
 * Defines the exporters this module implements.
 *
 * @return
 *   An "array of arrays", keyed by content-types. The 'handler' slot
 *   should point to the PHP class implementing this flag.
 */
function webform_digest_webform_exporters() {
  $exporters = array(
    'delimited_digest' => array(
      'title' => t('Delimited text digest'),
      'description' => t('A plain text file delimited by commas, tabs, or other characters.'),
      'digest' => TRUE,
      'handler' => 'webform_exporter_delimited_digest',
      'file' => drupal_get_path('module', 'webform_digest') . '/webform_digest_exporter_txt.inc',
      'weight' => 10,
    ),
    'excel_digest' => array(
      'title' => t('Microsoft Excel digest'),
      'description' => t('A file readable by Microsoft Excel.'),
      'digest' => TRUE,
      'handler' => 'webform_exporter_excel_xlsx_digest',
      'file' => drupal_get_path('module', 'webform_digest') . '/webform_digest_exporter_xlsx.inc',
      'weight' => -1,
      // Tells the options to use consistent dates instead of user-defined
      // formats.
      'options' => array(
        'iso8601_time' => TRUE,
        'iso8601_date' => TRUE,
      ),
    ),
  );

  // The new Excel exporter is only available if ZipArchive is available.
  if (!class_exists('ZipArchive')) {
    unset($exporters['excel_digest']);
  }

  return $exporters;
}

/**
 * Implements hook_preprocess_mimemail_message().
 */
function webform_digest_preprocess_mimemail_message(&$variables) {

  // Returning the default seems to be enough.
  template_preprocess_mimemail_message($variables);
}

/**
 * Implementation of hook_form_alter().
 */
function webform_digest_form_alter(&$form, &$form_state, $form_id) {

  // Alter the webform download form to remove digest format options.
  if ($form_id == 'webform_results_download_form') {
    foreach ($form['format']['#options'] as $format => $value) {
      if (strstr($format, '_digest')) {
        unset($form['format']['#options'][$format]);
      }
    }
  }
  // Alter the digest settings form.
  if ($form_id == 'webform_digest_form') {
    // Ensure that a default is selected the first time the form is loaded.
    if (!strstr($form['format']['#default_value'], '_digest')) {
      $form['format']['#default_value'] = 'delimited_digest';
    }
    foreach ($form['format']['#options'] as $format => $value) {
      if (!strstr($format, '_digest')) {
        unset($form['format']['#options'][$format]);
      }
    }
  }
}

/**
 * Implementation of hook_cron().
 */
function webform_digest_cron() {
  webform_digest_send_digest();
}

function webform_digest_send_digest($node = NULL) {
  module_load_include('inc', 'webform', 'includes/webform.report');
  $records = webform_digest_get_records($node);

  foreach ($records as $key => $record) {

    $to_send = false;
    if ($record->period == 'day') {
      if (time() - $record->sent > 60 * 60 && date('H') == $record->hourly_granularity) {
        $to_send = true;
      }
    }
    elseif ($record->period == 'week') {
      if (time() - $record->sent > 24 * 60 * 60 && date('w') == $record->daily_granularity && date('H') >= $record->hourly_granularity) {
        $to_send = true;
      }
    }
    elseif ($record->period == 'month') {
      if (time() - $record->sent > 24 * 60 * 60 && date('w') == $record->daily_granularity && date('H') >= $record->hourly_granularity) {
        $to_send = true;
      }
    }
    // If there's a node this is a test delivery so always send.
    if ($to_send || is_object($node)) {
      $settings = unserialize($record->settings);
      $form_state['values'] = $settings;
      $form_state['values']['node'] = node_load($key);
      $form_state['values']['format'] = $settings['format'];

      $form = array();
      webform_results_download_form_submit($form, $form_state);

      if ($batch = & batch_get() && !isset($batch['current_set'])) {
        $batch['programmed'] = 1;
        $batch['progressive'] = 0;
        $batch['webform_digest']['record'] = $record;

        // Use our own batch finished function
        $batch['sets'][0]['finished'] = 'webform_digest_results_batch_finished';

        batch_process();
      }
    }
  }
}

/**
 * Load digest records from the database.
 */
function webform_digest_get_records($node = NULL) {
  $query = db_select('webform_digest', 'wd');
  $query->fields('wd')
    ->condition('wd.enabled', 1);
  if (is_object($node)) {
    $query->condition('wd.nid', $node->nid);
  }
  $result = $query->execute();
  return $result->fetchAllAssoc('nid');
}

/**
 * Batch API completion callback; Record results in webform_digest table and
 * watchdog.
 */
function webform_digest_results_batch_finished($success, $results, $operations) {
  if ($success) {
    $record = array(
      'nid' => $results['node']->nid,
      'sent' => time(),
    );

    // For debugging it's easier to not save the success or the tester will have
    // to wait. This variable will need to be set manually. Test delivery should
    // not set the delivery time either.
    if (!variable_get('webform_digest_test_mode', 0) && !isset($results['node']->webform['digest_test_delivery'])) {
      drupal_write_record('webform_digest', $record, 'nid');
    }

    watchdog('webform_digest', 'Webform digest for !node sent.', array('!node' => l(check_plain($results['node']->title), 'node/' . $results['node']->nid)));
  }
  else {
    watchdog('webform_digest', 'Webform digest error sending !node.', array('!node' => l(check_plain($results['node']->title), 'node/' . $results['node']->nid), WATCHDOG_WARNING));
  }
}

/**
 * Load a digest config by nid.
 *
 * @param int $nid
 * @return array
 */
function webform_digest_is_enabled($nid) {
  $query = db_select('webform_digest', 'wd');
  $query->fields('wd')
    ->condition('wd.nid', $nid);
  $result = $query->execute();
  return $result->fetch();
}
