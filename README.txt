Webform digest enables automatic delivery of csv or Excel files of webform
results to designated recipients.

Webform digest requires the webform and mimemail modules. Mimemail has a further
dependency on mailsystem.

Changes from the D6 version:
* It's no longer necessary to patch the webform module.
* Recipents are added on the webform digest form and not configured on the
  webform download form.
* The sender will be the site default email and site name.

Instructions
* Cron must be enabled and working for automatic delivery.
* Edit a webform node and open the webform > digest tab.
* Check "Enable digest delivery" for this webform. You need to configure the
  digest for each webform that should have digest delivery.
* Check "Send only new data" to enable tracking of which records have been sent.
  You should set this in addition to the Download range options for only new
  submissions. If you are not able to set the download range options to only new
  because there are no unsent submissions, this will handle that.
* If you don't want to send only new data, you can set options in the Download
  range options at the bottom of the form.
* Configure the rest of the options and Save Configuration.
* The E-mail body field can accept most html (see filter_xss_admin()).
* You can test the delivery by clicking the Test delivery button.

Testing with cron
* In the case that you need to test delivery with cron multiple times before the
  time period is up, you can set a debugging variable 'webform_digest_test_mode'
  to 1 to prevent cron from saving the delivery time. Remember to reset this to
  0 or all webform digests will send on every cron run.

