<?php

/**
 * Return periods.
 */
function webform_digest_periods() {
  return array('day' => t('Daily'), 'week' => t('Weekly'), 'month' => t('Monthly'));
}

/**
 * Settings form.
 */
function webform_digest_form($form, &$form_state, $node) {

  $form = array();
  $query = db_select('webform_digest', 'wd');
  $query->fields('wd')
    ->condition('wd.nid', $node->nid);
  $result = $query->execute();
  $digest_conf = $result->fetchAssoc();

  // Define form elements.
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable digest delivery.'),
    '#default_value' => isset($digest_conf['enabled']) ? $digest_conf['enabled'] : 0,
    '#description' => t('If checkbox is checked, digest will be sent to email addresses specified in the E-mails settings page of Wefborm.'),
  );
  $form['new_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send only new data.'),
    '#default_value' => isset($digest_conf['new_data']) ? $digest_conf['new_data'] : 0,
    '#description' => t('If checkbox is checked, digest will include only new data since last email.'),
  );
  $periods = webform_digest_periods();
  $form['cron_notice'] = array(
    '#markup' => t('Cron must be configured for automatic digest emails.'),
  );
  $form['period'] = array(
    '#type' => 'select',
    '#title' => t('Frequency'),
    '#default_value' => isset($digest_conf['period']) ? $digest_conf['period'] : 0,
    '#options' => $periods,
    '#description' => t('Choose how often digest should be sent.'),
  );
  $form['daily_granularity_month'] = array(
    '#type' => 'select',
    '#title' => t('Day of month'),
    '#default_value' => isset($digest_conf['daily_granularity']) ? $digest_conf['daily_granularity'] : 1,
    '#options' => array_combine(range(1, 31), range(1, 31)),
    '#description' => t('Choose day of month when digest should be sent.'),
    '#states' => array(
      'visible' => array(
        ':input[name="period"]' => array('value' => 'month'),
      ),
    ),
  );
  $form['daily_granularity_week'] = array(
    '#type' => 'select',
    '#title' => t('Day of week'),
    '#default_value' => isset($digest_conf['daily_granularity']) ? $digest_conf['daily_granularity'] : 0,
    '#options' => array(
      '0' => t('Sunday'),
      '1' => t('Monday'),
      '2' => t('Tuesday'),
      '3' => t('Wednesday'),
      '4' => t('Thursday'),
      '5' => t('Friday'),
      '6' => t('Saturday'),
    ),
    '#description' => t('Choose day of week when digest should be sent.'),
    '#states' => array(
      'visible' => array(
        ':input[name="period"]' => array('value' => 'week'),
      ),
    ),
  );
  $form['hourly_granularity'] = array(
    '#type' => 'select',
    '#title' => t('Hour (24 hour clock)'),
    '#default_value' => isset($digest_conf['hourly_granularity']) ? $digest_conf['hourly_granularity'] : 0,
    '#options' => range(0, 23),
    '#description' => t('Choose time when digest should be sent (24 hour clock).'),
  );
  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail send list'),
    '#default_value' => isset($digest_conf['emails']) ? $digest_conf['emails'] : '',
    '#description' => t('Enter a list of email addresses to receive the digest. One per line.'),
    '#element_validate' => array('_webform_digest_validate_email'),
  );
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('E-mail body'),
    '#default_value' => isset($digest_conf['body']) ? $digest_conf['body'] : '',
    '#description' => t('Text that should be sent in e-mail.'),
  );
  $form['sent'] = array(
    '#type' => 'value',
    '#value' => $digest_conf['sent'],
  );

  // Load the settings we saved from the downloadd form state.
  $download_form_state['values'] = unserialize($digest_conf['settings']);
  $download_form_state['build_info']['args'][] = $node;

  // Get download form
  module_load_include('inc', 'webform', 'includes/webform.report');
  $download_form = call_user_func_array('drupal_retrieve_form', array('webform_results_download_form', &$download_form_state));

  // Set default values of fields in download form
  _webform_digest_set_default_value($download_form, $download_form_state);

  // Add $node to $form_state.
  $download_form_state['values']['node'] = $node;

  // Embed download form here.
  $form = array_merge($form, $download_form);

  // Unset webform's download button.
  unset($form['actions']);

  // Update the default range option if the digest setting is for new.
  $form['range']['range_type']['#default_value'] = $digest_conf['new_data'] == TRUE ? 'new' : 'all';

  // Override default state to show delimiter options for the digest format.
  $form['delimiter']['#states'] = array(
    'visible' => array(
      ':input[name=format]' => array('value' => 'delimited_digest'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );

  $form['test'] = array(
    '#type' => 'submit',
    '#value' => t('Test delivery'),
  );

  return $form;
}

/**
 * Settings form submit.
 */
function webform_digest_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Save configuration')) {

    // Create record object to save.
    $digest_conf = new stdClass();

    // Add nid to record.
    $digest_conf->nid = $form_state['values']['node']->nid;

    // Check period and add granularity field to record.
    if (isset($form_state['values']['daily_granularity_' . $form_state['values']['period']])) {
      $digest_conf->daily_granularity = $form_state['values']['daily_granularity_' . $form_state['values']['period']];
    }

    // Copy new data value to webform native setting because it can't be set on
    // the form if there are no unsent submissions.
    if ($form_state['values']['new_data']) {
      $form_state['values']['range']['range_type'] = 'new';
    }
    // Add other fields to record.
    $fields = array('enabled', 'new_data', 'period', 'sent', 'hourly_granularity', 'body', 'emails', 'daily_granularity_week', 'daily_granularity_month');
    foreach ($fields as $field) {
      $digest_conf->$field = $form_state['values'][$field];
    }

    // Unset fields from $form_state. We need to have only download form results here.
    $fields = array('enabled', 'new_data', 'period', 'sent', 'hourly_granularity', 'body', 'emails', 'daily_granularity_week', 'daily_granularity_month', 'node', 'reset', 'test');
    foreach ($fields as $field) {
      unset($form_state['values'][$field]);
      unset($form_state['clicked_button']['#post'][$field]);
    }

    // Unset empty components.
    foreach ($form_state['values']['components'] as $key => $value) {
      if (empty($value)) {
        unset($form_state['values']['components'][$key]);
      }
    }

    // Add $form_state values of download form to record.
    $digest_conf->settings = $form_state['values'];

    // Update record in database.
    db_delete('webform_digest')->condition('nid', array($digest_conf->nid))->execute();
    drupal_write_record('webform_digest', $digest_conf);
  }
  elseif ($form_state['values']['op'] == t('Reset')) {
    db_delete('webform_digest')->condition('nid', array($form_state['values']['node']->nid))->execute();
  }
  elseif ($form_state['values']['op'] == t('Test delivery')) {
    $form_state['values']['node']->webform['digest_test_delivery'] = TRUE;
    webform_digest_send_digest($form_state['values']['node']);
  }
}

/**
 * Helper function to set default values of fields in download form.
 */
function _webform_digest_set_default_value(&$download_form, $download_form_state) {
  $results = array();
  $children = element_children($download_form);
  foreach ($children as $key) {
    $child = $download_form[$key];
    if (is_array($child)) {
      if ($child['#type'] == 'fieldset') {
        $sub_children = element_children($child);
        foreach ($sub_children as $sub_key) {
          $sub_child = $download_form[$key][$sub_key];
          if (isset($download_form_state['values'][$key][$sub_key])) {
            $download_form[$key][$sub_key]['#default_value'] = $download_form_state['values'][$key][$sub_key];
          }
          elseif (isset($download_form_state['values'][$sub_key])) {
            $download_form[$key][$sub_key]['#default_value'] = $download_form_state['values'][$sub_key];
          }
        }
      }
      elseif (!empty($child['#type']) && isset($download_form_state['values'][$key])) {
        $download_form[$key]['#default_value'] = $download_form_state['values'][$key];
      }
   //   $results = array_merge($results, _webform_digest_set_default_value($child));
    }
  }
}


/**
 * A Drupal Form API Validation function. Validates the entered values from
 * email components on the client-side form.
 *
 * @param $form_element
 *   The e-mail form element.
 * @param $form_state
 *   The full form state for the webform.
 * @return
 *   None. Calls a form_set_error if the e-mail is not valid.
 */
function _webform_digest_validate_email($form_element, &$form_state) {
  $values = _webform_digest_explode_emails($form_element['#value']);
  foreach ($values as $value) {
    trim($value);
    if ($value !== '' && !valid_email_address($value)) {
      form_error($form_element, t('%value is not a valid email address.', array('%value' => $value)));
    }
  }
}

function _webform_digest_explode_emails($email) {
  $clean = array();
  $emails = str_replace("\r\n", "\n", trim($email));
  foreach (explode("\n", $emails) as $address) {
    $clean[] = trim($address);
  }
  return $clean;
}
