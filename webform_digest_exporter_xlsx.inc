<?php

module_load_include('inc', 'webform', 'includes/exporters/webform_exporter');
module_load_include('inc', 'webform', 'includes/exporters/webform_exporter_excel_xlsx');

class webform_exporter_excel_xlsx_digest extends webform_exporter_excel_xlsx {

  function __construct($options) {
    parent::__construct($options);
  }

  function add_row(&$file_handle, $data, $row_count) {
    parent::add_row($file_handle, $data, $row_count);
  }

  function download($node) {
    module_load_include('inc', 'webform_digest');

    // Get record from database.
    $digest_conf = webform_digest_is_enabled($node->nid);

    // Load the file here.
    $export_name = _webform_safe_name($node->title);
    if (!strlen($export_name)) {
      $export_name = t('Untitled');
    }

    // @todo figure out why this is called twice when testing. For now,
    // when testing this will get us back to the config page.
    if (!file_exists($this->options['file_name'])) {
      header("location: /node/$node->nid/webform/digest");
      return;
    }

    $file_content = file_get_contents(drupal_realpath($this->options['file_name']));

    // Clean up, the @ makes it silent.
    @unlink($this->options['file_name']);

    // Prepare array of emails to send.
    $node->webform['emails'] = db_select('webform_emails')
      ->fields('webform_emails')
      ->condition('nid', $node->nid)
      ->execute()
      ->fetchAllAssoc('eid', PDO::FETCH_ASSOC);

    $emails = _webform_digest_explode_emails($digest_conf->emails);

    // We don't need any headers.
    header_remove();

    // Prepare email data.
    $sender = array();
    $subject = 'Webform digest: ' . $node->title;
    $message = "<p>" . t('Webform digest is in attachment.') . "<p>\r\n";
    if (empty($this->options['sids']) && $this->options['range']['range_type'] != 'all') {
      $message .= "<p>" . t('No submissions to include.') . "</p>\r\n";
    }

    $file = array(
      'filename' => "$export_name.xlsx",
      'filemime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'filecontent' => $file_content
    );

    $message .= '<div>' . filter_xss_admin($digest_conf->body) . '</div>';

    foreach ($emails as $email) {
      $mail = array(
        'module' => 'webform_digest',
        'key' => 'webform_digest',
        'to' => $email,
        'from' => $sender,
        'subject' => $subject,
        'body' => $message,
        'headers' => array(),
        'params' => array(
          'headers' => array(),
          'attachments' => array($file),
        ),
      );
      $mail = mimemail_prepare_message($mail);
      $system = drupal_mail_system('mimemail', time());
      $sent = $system->mail($mail);
    }
    if ($sent) {
      // Update last sent.
      if (isset($this->options['sids']) && is_array($this->options['sids'])) {
        $this->last_sid = array_pop($this->options['sids']);
      }
    }
  }

  /**
   * Called from webform's batch post processing function. We interrupt webform
   * to send the emails using the same download method used in the test delivery
   * form submission.
   *
   * @param array $results
   *   The batch results.
   */
  function post_process(&$results) {
    parent::post_process($results);
    $this->download($results['node']);
    $this->update_sids($results['node']);
  }

  /**
   * Taken partially from webform_results_download(). Record the latest sid.
   */
  function update_sids($node) {
    // If anon user this is cron running, else it's a test delivery so save uid.
    global $user;
    $uid = $user->uid === 0 ? 0 : $user->uid;

    // Update user last downloaded sid if required.
    if ((isset($this->options['sids']) || $this->options['range']['range_type'] != 'range') && !empty($this->last_sid)) {
      // Delete existing record.
      db_delete('webform_last_download')
        ->condition('nid', $node->nid)
        // Cron should always be anon user.
        ->condition('uid', $uid)
        ->execute();
      // Write new record.
      db_insert('webform_last_download')
        ->fields(array(
          'nid' => $node->nid,
          'uid' => $uid,
          'sid' => $this->last_sid,
          'requested' => REQUEST_TIME,
        ))
        ->execute();
    }
  }

}
